export interface Music {
  title: string;
  v: string;
  artist?: string;
  description?: string;
}