## Build & Deploy to AWS Lambda

1. Run `build.sh` to create Go binary & ZIP package the required files.
2. Run `deploy.sh` to call the AWS CLI to deploy the package
